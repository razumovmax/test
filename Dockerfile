FROM ubuntu:20.04

ARG NGINX_VERSION=1.18.0

WORKDIR /nginx_build

RUN apt update \
    && apt install -y \
           build-essential \
           zlib1g \
           zlib1g-dev \
           libssl-dev \
           libpcre3 libpcre3-dev \
           wget \
    && wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
    && tar -xzvf ./nginx-${NGINX_VERSION}.tar.gz \
    && rm -rf ./nginx-${NGINX_VERSION}.tar.gz \
    && cd nginx-${NGINX_VERSION} \
    && ./configure \
           --prefix=/usr/share/nginx \
           --sbin-path=/usr/sbin/nginx \
           --conf-path=/etc/nginx/nginx.conf \
           --pid-path=/usr/local/nginx/nginx.pid \
           --with-http_ssl_module \
           --with-http_ssl_module \
           --with-pcre \
           --with-http_random_index_module \
           --user=nginx \
           --group=nginx \
           --without-mail_pop3_module \
           --without-mail_imap_module \
           --without-mail_smtp_module \
           --with-cc-opt='-g -O2' \
    && make \
    && make install \
    && rm -rf ../nginx-${NGINX_VERSION} \
    && apt remove -y \
           libssl-dev  \
           zlib1g-dev \
           libpcre3-dev \
           build-essential \
           wget \
    && apt clean autoclean -y \
    && apt autoremove -y \
    && rm -rf /var/lib/{apt,dpkg,cache,log}
    
WORKDIR /
COPY entrypoint.sh /entrypoint.sh
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf
COPY photo /photo

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

